(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function parseToFloat(e) {
    e2 = parseToInt(e2);
    if (0 <= e2 && e2 <= 5) return e2;
    if (6 <= e2 && e2 <= 10) return e2 / 2;
    if (11 <= e2 && e2 <= 50) return e2 / 10;
    if (51 <= e2 && e2 <= 99) return e2 / 20;
    if (e2 >= 100) return e2 / 100;
    return e2;
  }

  function normalize(str) {
    str = str.toString().replace(/\t/g, "").replace(/\n/g, "");
    return str;
  }

  function productPage() {
    var pageType = document.querySelector(".product-container");
    pageType = pageType && pageType.className.match("product-container");
    if (pageType) return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    // sku
    // i could'nt find a sample to work on...
    productInfo.sku = null;

    // expiration
    // this website does'nt use expiratoin
    // at least in this time...
    productInfo.expiration = null;

    //title
    var title = document.querySelector(".product-title");
    title = title && normalize(title.innerHTML);
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var image = document.querySelector(".product-image-element");
    image = image && image.src;
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // price
    var price = document.querySelector(
      ".product-controls .product-price-value"
    );
    price = price && parseToInt(price.innerText);
    if (price) productInfo.price = price;
    else productInfo.price = 0;

    // isAvailable
    var isAvailable = document.querySelector(".product-availability");
    isAvailable = isAvailable && isAvailable.textContent.match("آماده ارسال");
    if (isAvailable) productInfo.isAvailable = true;
    else productInfo.isAvailable = false;

    // discount
    var discount = document.querySelector(".product-discount-value");
    discount = discount && parseToFloat(discount.textContent);
    if (discount) productInfo.discount = discount;
    else productInfo.discount = null;

    // category
    var category = [];
    var tmpCategory = document.querySelectorAll(".breadcrumb li a");
    for (var i = 1; i < tmpCategory.length - 1; i++) {
      category.push(normalize(tmpCategory[i].textContent));
    }
    if (category) productInfo.category = category;

    // brand
    var brand = document.querySelector(".product-seller .pr-10");
    brand = brand && brand.textContent.replace("فروشنده: ", "");
    if (brand) productInfo.brand = brand;
    else productInfo.brand = null;

    // totalVotes
    var totalVotes = document.querySelector(".product-rating-info small");
    if (totalVotes) productInfo.totalVotes = parseToInt(totalVotes.textContent);
    else productInfo.totalVotes = null;

    // averageVote
    var averageVote = document.querySelector(".product-rating");
    averageVote = averageVote && averageVote.attributes[1].value.split(",")[1];
    if (averageVote && totalVotes)
      productInfo.averageVote = parseToFloat(averageVote);
    else productInfo.averageVote = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
    if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
