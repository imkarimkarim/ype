(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function productPage() {
    var pageType = document.querySelector("body");
    if (pageType.id == "product") return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    // sku
    var sku = document.querySelector('input[name="id_product"]');
    var backupSku = document.querySelector(".btn-ccc");
    sku = (sku && sku.value) || (backupSku && backupSku.dataset.idProduct);
    if (sku) productInfo.sku = sku;
    else productInfo.sku = null;

    //title
    var title = document.querySelector(".product_name h1");
    var backupTitle = document.querySelector(".navigation-pipe");
    title =
      (title && title.innerText) ||
      (backupTitle && backupTitle.nextSibling.data);
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var image = document.getElementById("bigpic");
    image = image && image.src;
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // price
    var price = document.getElementById("our_price_display");
    price = price && parseToInt(price.innerHTML);
    if (price) productInfo.price = price;
    else productInfo.price = null;

    // isAvailable
    if (productInfo.price > 0) productInfo.isAvailable = true;
    else productInfo.isAvailable = false;

    // category
    var category = document.querySelectorAll(".breadcrumb li a");
    if (category) productInfo.category = category[1].innerHTML;
    else productInfo.category = null;

    // brand
    var brand = document.getElementById("collapsesheet");
    brand = brand && brand.firstChild.firstChild.lastChild.innerHTML;
    if (brand) productInfo.brand = brand;
    else productInfo.brand = null;

    // discount
    // this website does'nt use discount
    // at least in this time...
    productInfo.discount = null;

    // expiration
    // this website does'nt use expiratoin
    // at least in this time...
    productInfo.expiration = null;

    // averageVote
    // i could'nt find a sample to work on...
    productInfo.averageVote = null;

    // totalVotes
    // i could'nt find a sample to work on...
    productInfo.totalVotes = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.sku && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
    if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
