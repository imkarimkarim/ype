(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function parseToFloat(e2) {
    e2 = parseToInt(e2);
    return e2;
  }

  function calculateDiscount(price, oldPrice) {
    price = parseToInt(price);
    oldPrice = parseToInt(oldPrice);
    return parseToFloat(100 - (price * 100) / oldPrice);
  }

  function productPage() {
    var pageType = document.querySelector('meta[property="og:type"]');
    if (pageType.content == "product") return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    var jsonImage = null;
    var jsonProduct = null;
    try {
      var ldJsons = document.querySelectorAll(
        'script[type="application/ld+json"]'
      );
      ldJsons.forEach(function (item) {
        item = JSON.parse(item.innerText);
        item = item["@graph"];
        for (var i = 0; i < item.length; i++) {
          if (item[i]["@type"] == "ImageObject") jsonImage = item[i].url;
          if (item[i]["@type"] == "Product") jsonProduct = item[i];
        }
      });
    } catch (error) {
      console.error(error);
    }

    // sku
    var sku = document.querySelector('button[name="add-to-cart"]');
    sku = sku && sku.value;
    if (sku) productInfo.sku = sku;
    else productInfo.sku = null;

    //title
    var title = document.querySelector(".product_title");
    var backupTitle = jsonProduct && jsonProduct.name;
    title = (title && title.innerText) || backupTitle;
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var backupImage = null;
    var image = document.querySelector(".zoomImg");
    if (jsonImage) backupImage = jsonImage;
    image = (image && image.src) || backupImage;
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // isAvailable
    var isAvailable = document.querySelector(
      'meta[property="product:availability"]'
    );
    if (isAvailable && isAvailable.content == "instock")
      productInfo.isAvailable = true;
    else productInfo.isAvailable = false;

    // price
    var price = document.querySelector('meta[property="product:price:amount"]');
    var backupPrice = document.querySelector(".price ins");
    price =
      (price && parseToInt(price.content)) ||
      (backupPrice && parseToInt(backupPrice.innerText));
    if (price) productInfo.price = price;
    else productInfo.price = null;

    // discount
    var discount = document.querySelector(".woodmart-price-outside .onsale");
    var backupDiscount = document.querySelector(
      ".woodmart-price-outside .price del"
    );
    if (productInfo.price && backupDiscount) {
      backupDiscount = calculateDiscount(
        productInfo.price,
        backupDiscount.innerText
      );
    }
    discount = (discount && parseToFloat(discount.innerText)) || backupDiscount;
    if (discount) productInfo.discount = discount;
    else productInfo.discount = null;

    // category
    var category = [];
    var tmpCategory = document.querySelectorAll(".woocommerce-breadcrumb a");
    for (var i = 1; i < tmpCategory.length; i++) {
      category.push(tmpCategory[i].innerHTML);
    }
    if (category) productInfo.category = category;

    // brand
    var brand = null;
    if (jsonProduct.brand) brand = jsonProduct.brand.name;
    if (brand) productInfo.brand = brand;
    else productInfo.brand = null;

    // expiration
    // this website does'nt use expiratoin
    // at least in this time...
    productInfo.expiration = null;

    // totalVotes
    var totalVotes = document.querySelector(".woocommerce-review-link span");
    if (totalVotes) productInfo.totalVotes = parseToInt(totalVotes.innerHTML);
    else productInfo.totalVotes = null;

    // averageVote
    var averageVote = document.querySelector(".star-rating span");
    if (averageVote && productInfo.totalVotes)
      productInfo.averageVote = parseToFloat(
        averageVote.firstElementChild.innerHTML
      );
    else productInfo.averageVote = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.sku && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
    if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
