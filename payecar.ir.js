(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function productPage() {
    var pageType = document.querySelector("body");
    if (pageType.id == "product") return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    // sku
    var sku = document.querySelector('input[name="id_product"]');
    sku = sku && (sku.value);
    if (sku) productInfo.sku = sku;
    else productInfo.sku = null;

    //title
    var title = document.querySelectorAll("h1");
    var backupTitle = document.querySelectorAll(".jqzoom");
    title = title && title[0].innerHTML || backupTitle && backupTitle[0].title;
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var image = document.querySelector(".jqzoom");
    var backupImage = document.querySelector(".zoomPad");
    image = image && image.href || backupImage && backupImage.firstChild.attributes.src.nodeValue;
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // price
    var price = document.getElementById("our_price_display");
    var backupPrice = document.querySelectorAll('script[type="text/javascript"]');

    backupPrice.forEach(function (script) {
      if (script.src == "") {
        script = script.innerHTML.match(/productPrice=\d+/g);
        if (script) {
          backupPrice = script[0];
        }
      }
    });

    price =
      (price && parseToInt(price.innerHTML)) || (backupPrice && parseToInt(backupPrice));
    if (price && price != 1) productInfo.price = price;
    else productInfo.price = null;

    // isAvailable
    if (productInfo.price) productInfo.isAvailable = true;
    else productInfo.isAvailable = false;

    // category
    var category = document.querySelectorAll(".navigation_page span a");
    productInfo.category = [];
    for (var i = 0; i < category.length; i++) {
      productInfo.category.push(category[i].title);
    }

    // brand
    var brand = document.querySelector(".brand a img");
    var backupBrand = document.querySelectorAll("#collapsesheet div div span");
    brand = (brand && brand.alt) || (backupBrand && backupBrand[0].innerHTML);
    if (brand) productInfo.brand = brand;
    else productInfo.brand = null;

    // discount
    // this website does'nt use discount
    // at least in this time...
    productInfo.discount = null;

    // expiration
    // this website does'nt use expiratoin
    // at least in this time...
    productInfo.expiration = null;

    // averageVote
    // i could'nt find a sample to work on...
    productInfo.averageVote = null;

    // totalVotes
    // i could'nt find a sample to work on...
    productInfo.totalVotes = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.sku && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
     if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
